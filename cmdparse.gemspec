#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-
# stub: cmdparse 3.0.7 ruby lib

Gem::Specification.new do |s|
  s.name = "cmdparse".freeze
  s.version = "3.0.7"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Thomas Leitner".freeze]
  s.date = "2020-12-08"
  s.description = "       cmdparse provides classes for parsing (possibly nested) commands on the command line;\n       command line options themselves are parsed using optparse.\n".freeze
  s.email = "t_leitner@gmx.at".freeze
  s.files = ["COPYING".freeze, "README.md".freeze, "Rakefile".freeze, "VERSION".freeze, "doc/api.api".freeze, "doc/api.template".freeze, "doc/default.scss".freeze, "doc/default.template".freeze, "doc/images/bg01.png".freeze, "doc/images/bg02.png".freeze, "doc/index.page".freeze, "doc/installation.page".freeze, "doc/metainfo".freeze, "doc/news.page".freeze, "doc/sitemap.sitemap".freeze, "doc/tutorial.page".freeze, "doc/virtual".freeze, "example/net.rb".freeze, "lib/cmdparse.rb".freeze, "setup.rb".freeze, "webgen.config".freeze]
  s.homepage = "https://cmdparse.gettalong.org".freeze
  s.licenses = ["MIT".freeze]
  s.rdoc_options = ["--line-numbers".freeze, "--main".freeze, "CmdParse::CommandParser".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.0.0".freeze)
  s.rubygems_version = "2.5.2.1".freeze
  s.summary = "Advanced command line parser supporting commands".freeze

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<webgen>.freeze, ["~> 1.4"])
    else
      s.add_dependency(%q<webgen>.freeze, ["~> 1.4"])
    end
  else
    s.add_dependency(%q<webgen>.freeze, ["~> 1.4"])
  end
end
